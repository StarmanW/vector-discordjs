'use strict';

/**
 * ConfigurationError class.
 * This class extends the Error class.
 * @class
 */
class ConfigurationError extends Error {
    /**
     * Constructor method
     * @constructor
     * @param {string} message 
     */
    constructor(message) {
        super(message);
        this.name = 'Configuration Error';
    }

    /**
     * Returns a formatted representation
     * of the error message and name.
     * @returns {string}
     */
    toString() {
        return `${this.name}: ${this.message}`;
    }
}

module.exports = ConfigurationError;