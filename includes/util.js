'use stict';

const ConfigurationError = require('./configurationError');

/**
 * Utility class
 * @class
 */
class Util {
    /**
     * Pad zero infront of date/time
     * @static
     * @param {string} dateTime 
     * @returns {(string|number)} Returns padded zero string or defaults to number.
     */
    static padZero(dateTime) {
        return (dateTime < 10) ? `0${dateTime}` : dateTime;
    }

    /**
     * Static method to get config
     * @static
     * @throws {Error} Throws an Error if config.json file is not found.
     * @throws {ConfigurationError} Throws ConfigurationError if config file is not configured properly.
     * @returns {Object} Returns the configuration object from config.json file.
     */
    static getConfig() {
        try {
            const config = require('../config.json'),
                ERR_MSG = {
                    prefix: 'Please set a default prefix for the bot. Preferrably a unique one and won\'t conflict with other bot\'s prefix on the same server.',
                    token: 'Please provide your bot token. For more information, go to https://www.discordapp.com/developers and generate a token.',
                    bot_owner: 'Please provide a bot owner ID in the config.json file.',
                    check_cid: 'Please provide the check channel ID.'
                };

            // Check for empty value
            Object.entries(config).forEach((v) => {
                if (v[1] === '') {
                    throw new ConfigurationError(ERR_MSG[v[0]]).toString();
                }
            });

            // Return config object if no error
            return config;
        } catch (err) {
            throw new ConfigurationError(err.message).toString();
        }
    }
}

module.exports = Util;