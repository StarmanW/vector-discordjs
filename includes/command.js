'use strict';

/**
 * Command class.
 * This is the base class for all the commands.
 * @class
 */
class Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client, help) {
        this.client = client;
        this.help = help;
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {String} args 
     */
    async run(message, args) {
        throw new Error('Please override this method in the subclass.');
    }
}

module.exports = Command;