'use strict';

const { Client, Collection } = require('discord.js'),
    Util = require('./util'),
    Logger = require('../modules/Logger'),
    path = require('path'),
    fs = require('fs');

/**
 * Vector class that extends the main
 * Client class from discord.js.
 * @class
 */
class Vector extends Client {
    /**
     * Constructor method
     * @constructor
     * @param {ClientOptions} options 
     */
    constructor(options) {
        super(options);
        this.config = Util.getConfig();
        this.commands = new Collection();
        this.logger = Logger;
    }

    /**
     * Initialization of commands and events for 
     * the bot. After initialization has 
     * completed, login the bot.
     */
    run() {
        try {
            this._initCommands();
            this._initEvents();
            this.login(this.config.token);
        } catch (err) {
            this.logger.error(`${err.name}: ${err.message}`);
        }
    }

    /**
     * Performs initialization of bot commands.
     * @private
     */
    _initCommands() {
        this.logger.log('Loading commands...')

        fs.readdir(path.resolve(path.join(__dirname, '../commands/')), (err, files) => {
            if (err) this.logger.error(`${err.name}: ${err.message}`);

            // Filter out non-JS files
            let jsFiles = files.filter(f => f.endsWith('.js'));

            // Log message if no command files available
            if (jsFiles.length <= 0) {
                return this.logger.log('No commands loaded.');
            }

            // Register each commands to bot.commands
            jsFiles.forEach((f) => {
                const command = new (require(`../commands/${f}`))(this);
                this.commands.set(command.help.name, command);
                this.logger.log(`Command: ${f} loaded.`);
            });

            this.logger.ready(`Successfully loaded ${jsFiles.length} commands!`);
        });
    }

    /**
     * Performs initialization of bot events.
     * @private
     */
    _initEvents() {
        this.logger.log('Loading events...');

        fs.readdir(path.resolve(path.join(__dirname, '../events/')), (err, files) => {
            if (err) return this.logger.error(`${err.name}: ${err.message}`);

            // Filter out non-JS files
            let jsFiles = files.filter(f => f.endsWith('.js'));

            // Log message if no command files available
            if (jsFiles.length <= 0) {
                return this.logger.log('No events loaded.');
            }

            // Register each event listeners
            jsFiles.forEach((f) => {
                const event = new(require(`../events/${f}`))(this),
                    eventName = f.split('.')[0];

                this.on(eventName, (...args) => event.run(...args));
                this.logger.log(`Event: ${f} loaded.`);
            });

            this.logger.ready(`Successfully loaded ${jsFiles.length} events!`);
        });
    }
}

module.exports = Vector;