'use strict';

const Command = require("../includes/command.js"),
    RichEmbed = require("discord.js").RichEmbed,
    Util = require('../includes/util');

/**
 * ServerInfo class
 * Get the information about this server.
 * @class
 */
class ServerInfo extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: "serverinfo",
            category: 'UTILITY',
            description: 'Get the information about this server.',
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {String} args 
     */
    async run(message, args) {
        try {
            // Create server info using RichEmbed
            let serverInfoEmbed = new RichEmbed()
                .setThumbnail(message.guild.iconURL)
                .setTitle('Owner')
                .setDescription(`<@${message.guild.owner.id}>`)
                .addField('Server ID', message.guild.id, true)
                .addField('Server Name', message.guild.name, true)
                .addField('Created On', this._getCreatedTime(message.guild.createdTimestamp), true)
                .addField('Total Roles', `${message.guild.roles.size} role(s)`, true)
                .addField('Total Members', this._getMembers(message.guild.members), true)
                .addField('Total Channels', this._getChannels(message.guild.channels), true)
                .setColor('AQUA');

            // Send server info embed message
            message.channel.send(serverInfoEmbed);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }

    /**
     * Get formatted timestamp of guild creation
     * @private
     * @param {Number} timestamp 
     * @returns {String}
     */
    _getCreatedTime(timestamp) {
        let createdAt = new Date(timestamp);

        return `${Util.padZero(createdAt.getDate())}/${Util.padZero(createdAt.getMonth())}/${createdAt.getFullYear()} - ${Util.padZero(createdAt.getUTCHours())}:${Util.padZero(createdAt.getUTCMinutes())} UTC`;
    }

    /**
     * Get formatted members of guild
     * @private
     * @param {Collection<GuildMember>} guildMembers 
     * @returns {String}
     */
    _getMembers(members) {
        // Define counter variable
        let memberCount = members.size,
            onlineCount = members.filter(m => m.presence.status === 'online').size,
            botCount = members.filter(m => m.user.bot === true).size;

        return `${memberCount} member(s)\n${onlineCount} online\n${botCount} bot(s), ${memberCount - botCount} human(s)`;
    }

    /**
     * Get formatted channels of guild
     * @private
     * @param {Collection<GuildChannel>} guildChannels 
     * @returns {String}
     */
    _getChannels(channels) {
        // Define counter variable
        let categoryCount = channels.filter((c) => c.type === 'category').size,
            textCount = channels.filter((c) => c.type === 'text').size,
            voiceCount = channels.filter((c) => c.type === 'voice').size;

        return `${categoryCount} categorie(s)\n${textCount} text channel(s)\n${voiceCount} voice channel(s)`;
    }
}

module.exports = ServerInfo;