'use strict';

const Command = require("../includes/command.js");

/**
 * Clean class
 * Delete message on a channel
 * @class
 */
class Clean extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'clr',
            category: 'UTILITY',
            description: 'Clean messages on a channel. If amount of messages is specified, it will clean the amount of messages based on the amount given. Otherwise it will clean the last 50 messages by default.',
            example: '<amount>(optional)'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        // Check if the user fired the command has manage messages rights
        // Check if bot has 'manage_messages' rights
        if (!message.member.hasPermission('MANAGE_MESSAGES')) return;
        if (!message.guild.members.get(this.client.user.id).hasPermission('MANAGE_MESSAGES')) {
            return message.channel.send('I do not have the permission to manage message :slight_frown:');
        }

        // Invalid arguments check
        // More than one argument && argument format does not match
        if (args.length !== 0 && !/^[1-9]+$/.test(args[0])) return;

        try {
            // Get number of messages to delete, else default to 50
            // Fetch messages based on limit
            const limit = parseInt(args[0]) || 50,
                messages = await message.channel.fetchMessages({
                    limit: parseInt(limit) + 1
                });

            // Return if no message to delete (1 is the command sent from user)
            if (messages.size <= 1) return;

            // Bulk delete messages and send a status message
            const deletedMsgs = await message.channel.bulkDelete(messages),
                statusMsg = await message.reply(`Successfully deleted ${deletedMsgs.size - 1} message(s).`);

            // Delete status message
            statusMsg.delete(3000);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Clean;