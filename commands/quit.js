'use strict';

const Command = require("../includes/command.js");

/**
 * Quit class
 * Quit the bot
 * @class
 */
class Quit extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: "quit",
            category: 'UTILITY',
            description: 'Quit the bot. This command can only be issued by the owner.',
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        try {
            // Check if sender is the bot owner
            if (message.author.id !== this.client.config.bot_owner) return;

            let msg = await message.channel.send('Bot quitting now');

            // Append '.' for fancy visual effects
            for (let i = 0; i < 3; i++) {
                msg = await msg.edit(msg += '.');
            }

            await message.channel.send('Bye!');

            // Logout and close the connection
            this.client.destroy();
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Quit;