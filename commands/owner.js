'use strict';

const RichEmbed = require('discord.js').RichEmbed,
    Command = require("../includes/command.js");

/**
 * Owner class
 * Display summarized information about the bot owner.
 * @class
 */
class Owner extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'owner',
            category: 'UTILITY',
            description: 'Display summarized information about the bot owner.',
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        try {
            // Fetch user based on ID set in config file
            const owner = await this.client.fetchUser(this.client.config.bot_owner),
                ownerAbout = 'He likes foods especially pizza! :pizza: :cooking: :ramen:';

            // Create owner embed message
            const ownerEmbed = new RichEmbed()
                .setTitle('Name')
                .setDescription(`${owner.username}#${owner.discriminator}`)
                .setThumbnail(`${owner.displayAvatarURL}`)
                .addField('About', ownerAbout)
                .setColor('RANDOM');

            // Send message
            message.channel.send('Meet my owner!', ownerEmbed);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Owner;