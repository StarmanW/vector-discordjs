'use strict';

const RichEmbed = require('discord.js').RichEmbed,
    Command = require("../includes/command.js");

/**
 * UserInfo class
 * Display the information of a specified user.
 * @class
 */
class UserInfo extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'userinfo',
            category: 'UTILITY',
            description: 'Display the information of a specified user. Accepts user ID, username, user nickname and user mentions.',
            example: '<user_id|username|nickname>'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        // Join arguments by space
        const user = args.join(' ');
        let userInfo = undefined;

        try {
            // If user is not supplied, send help message
            if (!user) {
                let helpMsg = await message.channel.send(`Please provide me a user.\nUsage: \`${this.client.config.prefix}userinfo <user_id|nickname|username>\`\nExample: \`\`\`${this.client.config.prefix}userinfo UserName\`\`\``);
                return await helpMsg.delete(5000);
            }

            /**
             * Retieve user based on argument given.
             * - user id|@mention|@nickname_mention
             * - username|nickname
             */
            if (/^(<@!?)?\d+>?$/.test(user)) {
                userInfo = message.guild.members.get(user.replace(/^(<@!?)?(\d+)>?$/, '$2'));
            } else {
                userInfo = message.guild.members.find(usr => usr.displayName === user || usr.user.username === user);
            }

            // If user is not found
            if (!userInfo) {
                return message.channel.send('I can\'t find that user :thinking:. Must\'ve been kidnapped by the Wumpus!')
            }

            // Create user embed using RichEmbed
            const userEmbed = new RichEmbed()
                .setTitle('Username')
                .setDescription(`${userInfo.user.username}#${userInfo.user.discriminator}`)
                .addField('Nickname', `${userInfo.nickname || 'Nickname not set on this server'}`, true)
                .addField('Joined at', `${userInfo.joinedAt}`)
                .addField('Account Created at', `${userInfo.user.createdAt}`)
                .setThumbnail(userInfo.user.displayAvatarURL)
                .setColor('BLUE');

            // Send user info
            await message.channel.send(userEmbed);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = UserInfo;