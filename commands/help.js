'use strict';

const RichEmbed = require('discord.js').RichEmbed,
    Command = require("../includes/command.js");

/**
 * Help class
 * Get the help information of the commands
 * @class
 */
class Help extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'help',
            category: 'UTILITY',
            description: 'Display the help information for all the commands.',
            example: '<Command>(optional)'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        try {
            // Fetch user based on ID set in config file
            const owner = await this.client.fetchUser(this.client.config.bot_owner),
                DESCRIPTION = `I am an experimental bot developed by ${owner.username}#${owner.discriminator}. My core is built using Javascript with awesome juices from DiscordJS!\n\nHere are the list of my commands:`,
                cmds = {},
                helpMsg = new RichEmbed();

            // If a specific command help is requested
            if (args.length && this.client.commands.has(args[0])) {
                // Get information of the specified command
                let cmd = this.client.commands.get(args[0]);

                helpMsg.addField('Command Name', `\`${this.client.config.prefix}${cmd.help.name}\``, true)
                    .addField('Command Category', `**${cmd.help.category}**`, true)
                    .addField('Command Description', cmd.help.description)
                    .addField('Example', `${this.client.config.prefix}${cmd.help.name} ${cmd.help.example}`)
                    .setColor('RED');

                return message.channel.send(helpMsg);
            }

            // Add each commands into 'cmds' object
            this.client.commands.forEach((c) => {
                if (!(c.help.category in cmds)) {
                    cmds[c.help.category] = `\`${this.client.config.prefix}${c.help.name}\``;
                } else {
                    cmds[c.help.category] += `, \`${this.client.config.prefix}${c.help.name}\``;
                }
            });

            // Create help embed message
            helpMsg.setAuthor('Hello, Vector here!')
                .setThumbnail(this.client.user.avatarURL)
                .setDescription(DESCRIPTION)
                .setColor('AQUA');

            // Add each command to help embed message
            Object.entries(cmds).forEach(cmd => helpMsg.addField(cmd[0], cmd[1]));
            helpMsg.addField('Commands Help', `Use \`${this.client.config.prefix}help <Command>\` for more information about a command.\n**Example:**\n\`${this.client.config.prefix}help icon\` for more information about the icon command.`);

            // Send help embedded message
            message.channel.send(helpMsg);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Help;