'use strict';

const Command = require("../includes/command.js"),
    Attachment = require('discord.js').Attachment;

/**
 * Icon class
 * Get the icon of the guild
 * @class
 */
class Icon extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'icon',
            category: 'UTILITY',
            description: 'Get the server\'s icon.',
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        try {
            message.channel.send(new Attachment(message.guild.iconURL, 'server_icon.png'));
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Icon;