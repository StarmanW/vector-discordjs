'use strict';

const Command = require("../includes/command.js"),
    Attachment = require('discord.js').Attachment;

/**
 * Avatar class
 * Get and display avatar of a user
 * @class
 */
class Avatar extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'avatar',
            category: 'UTILITY',
            description: 'Get avatar link of the specified user. Accepts user ID, username, user nickname and user mentions.',
            example: '<user_id|username|nickname>'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        // Join arguments by space
        const user = args.join(' ');
        let userInfo = undefined;

        try {
            // If user is not supplied, send help message
            if (!user) {
                let helpMsg = await message.channel.send(`Please provide me a user.\nUsage: \`${this.client.config.prefix}avatar <user_id|nickname|username>\`\nExample: \`\`\`${this.client.config.prefix}avatar UserName\`\`\``);
                return helpMsg.delete(5000);
            }

            /**
             * Retieve user based on argument given.
             * - user id|@mention|@nickname_mention
             * - username|nickname
             */
            if (/^(<@!?)?\d+>?$/.test(user)) {
                userInfo = message.guild.members.get(user.replace(/^(<@!?)?(\d+)>?$/, '$2'));
            } else {
                userInfo = message.guild.members.find(usr => usr.displayName === user || usr.user.username === user);
            }

            // If user is not found
            if (!userInfo) {
                return message.channel.send('Hmm that user is not in this server...:thinking:')
            }

            // Send user avatar as a file
            // message.channel.send(userInfo.user.displayAvatarURL);
            message.channel.send(new Attachment(userInfo.user.displayAvatarURL, 'avatar.png'));
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}


module.exports = Avatar;