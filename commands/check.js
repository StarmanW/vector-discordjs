'use strict';

const Command = require("../includes/command.js");

/**
 * Check class
 * React to mark a checkpoint on channel
 * specified in the config file.
 * @class
 */
class Check extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'check',
            category: 'OWNER ONLY',
            description: `React to mark a checkpoint on <#${client.config.check_cid}>`,
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        // Check for channel is the exact channel specified in config
        // React permission check
        if (message.channel.id !== this.client.config.check_cid || message.author.id !== this.client.config.bot_owner) return;
        if (!message.guild.members.get(this.client.user.id).hasPermission('ADD_REACTIONS')) {
            return message.channel.send('I do not have the permission to react :slight_frown:');
        }

        try {
            // Delete message
            await message.delete();

            // Fetch the message that is before the deleted message
            let msg = await message.channel.fetchMessages({
                limit: 1,
                before: message.channel.lastMessageID
            });

            // React on fetched message
            msg.first().react('👍');
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Check;