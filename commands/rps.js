'use strict';

const Command = require("../includes/command.js");

/**
 * RPS class
 * Play rock, paper, scissor with the bot
 * @class
 */
class RPS extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'rps',
            category: 'FUN',
            description: 'Play `rock`, `paper`, `scissor` with the bot.',
            example: '<rock|paper|scissor>'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        // Invalid/empty argument check
        if (!args.length) return message.channel.send('Please let me know your choice.\n**rock**, **paper** or **scissor**');
        if (!/^(rock|paper|scissor)$/.test(args[0].toLowerCase())) return message.channel.send('Booo, invalid choice! :stuck_out_tongue:');

        /**
         * RPS Rules:
         * Rock    - 0
         * Paper   - 1
         * Scissor - 2
         */
        const CHOICES = [
            'rock',
            'paper',
            'scissor'
        ];

        // Set user choice and bot choice into variable for comparison
        let userChoice = args[0].toLowerCase() === "rock" ? 0 : args[0].toLowerCase() === "paper" ? 1 : 2,
            botChoice = Math.floor(Math.random() * (2 + 1 - 0)) + 0;

        try {
            // Draw check
            if (userChoice === botChoice) {
                return message.channel.send(`Good game! I chose ${CHOICES[botChoice]}! It's a drawn.`);
            }

            // Rock/Scissor check
            if (userChoice === 0 && botChoice === 2) {
                return message.channel.send(`Lucky you! I chose ${CHOICES[botChoice]}, you won! :clap:`);
            } else if (botChoice === 0 && userChoice === 2) {
                return message.channel.send(`Boop Bo0p BoOop! I chose ${CHOICES[botChoice]}, I won! Better luck next time :wink:`);
            }

            // Other choices
            if (userChoice > botChoice) {
                return message.channel.send(`Awesome! I chose ${CHOICES[botChoice]}, you won! :thumbsup:`);
            } else {
                return message.channel.send(`Ha! I chose ${CHOICES[botChoice]}, I won! :stuck_out_tongue:`);
            }
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = RPS;