'use strict';

const Command = require("../includes/command.js");

/**
 * UpTime class
 * Get the uptime of the bot.
 * @class
 */
class UpTime extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: "uptime",
            category: 'UTILITY',
            description: 'Get the uptime of the bot.',
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {String} args 
     */
    async run(message, args) {
        try {
            // Break down time from this.client.uptime
            let totalSeconds = this.client.uptime / 1000,
                hours = Math.floor(totalSeconds / 3600),
                minutes = Math.floor((totalSeconds % 3600) / 60),
                seconds = Math.floor(totalSeconds % 60);

            // Send the uptime to channel
            message.channel.send(`Uptime: ${hours} hours ${minutes} minutes ${seconds} seconds`);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = UpTime;