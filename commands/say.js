'use strict';

const Command = require("../includes/command.js");

/**
 * Say class
 * Make the bot to say something.
 * @class
 */
class Say extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'say',
            category: 'FUN',
            description: 'Make the bot say something. It\'s fun!',
            example: 'Hello there!'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        try {
            // Delete message and send back the exact arguments
            await message.delete();
            message.channel.send(args.join(' '));
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Say;