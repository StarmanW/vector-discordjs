'use strict';

const Command = require("../includes/command.js");

/**
 * SetStatus class
 * Set the presence status of the bot.
 * @class
 */
class SetStatus extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'setstatus',
            category: 'UTILITY',
            description: 'Set the presence status of the bot. Accepts only online, invisible, idle and dnd.',
            example: '<online|invisible|idle|dnd>'
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        try {
            // Check if sender is the bot owner
            // Check for correct argument received
            if (message.author.id !== this.client.config.bot_owner) return;
            if (args.length !== 1 || !/^(dnd|invisible|idle|online)$/.test(args[0])) return;

            // Set bot status
            this.client.user.setStatus(args[0]);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = SetStatus;