'use strict';

const Command = require("../includes/command.js");

/**
 * Coin class
 * Toast a coin
 * @class
 */
class Coin extends Command {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        super(client, {
            name: 'coin',
            category: 'FUN',
            description: 'Toast a coin to get head or tail.',
            example: ''
        });
    }

    /**
     * Run method
     * @param {Message} message 
     * @param {Array} args 
     */
    async run(message, args) {
        const COIN = [
            'Head',
            'Tail'
        ];

        try {
            message.channel.send(`Coin Result: ${COIN[Math.floor((Math.random() * COIN.length))]}`);
        } catch (err) {
            this.client.logger.error(`${err.name}: ${err.message}`);
        }
    }
}

module.exports = Coin;