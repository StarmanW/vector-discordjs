'use strict';

const chalk = require("chalk"),
	moment = require("moment");

/**
 * Logger class
 * @class
 */
class Logger {
	/**
	 * Log method
	 * @static
	 * @param {string} content 
	 * @param {string} type 
	 */
	static log(content, type = "log") {
		const timestamp = `[${moment().format("DD-MMM-YYYY @ HH:mm:ss")}]:`;
		
		switch (type) {
			case "log":
				console.log(`${timestamp} ${chalk.bgBlue(type.toUpperCase())} ${content}`);
				break;
			case "warn":
				console.log(`${timestamp} ${chalk.black.bgYellow(type.toUpperCase())} ${content}`);
				break;
			case "error":
				console.log(`${timestamp} ${chalk.bgRed(type.toUpperCase())} ${content}`);
				break;
			case "ready":
				console.log(`${timestamp} ${chalk.black.bgGreen(type.toUpperCase())} ${content}`);
				break;
			default:
				throw new TypeError("Logger type must be either warn, debug, log, ready, cmd or error.");
		}
	}

	/**
	 * Warn method
	 * @static
	 * @param {string} content 
	 */
	static warn(content) {
		return this.log(content, "warn");
	}

	/**
	 * Error method
	 * @static
	 * @param {string} content 
	 */
	static error(content) {
		return this.log(content, "error");
	}

	/**
	 * Ready method
	 * @static
	 * @param {string} content 
	 */
	static ready(content) {
		return this.log(content, "ready");
	}
}

module.exports = Logger;