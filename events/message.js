'use strict';

/**
 * Message class
 * @class
 */
class Message {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        this.client = client;
    }

    /**
     * Run method
     */
    async run(message) {
        // A little bit of data parsing/general checks
        if (message.author.bot) return;
        if (message.channel.type === 'dm') return;
        if (!message.content.startsWith(this.client.config.prefix)) return;

        /**
         * Breakdown message contents
         * 
         * - ['prefix<Command>', ...args]
         * - prefix
         * - <Command>
         * - ...args
         */
        let content = message.content.split(/\s+/),
            prefix = this.client.config.prefix,
            command = content[0].slice(prefix.length),
            args = content.slice(1);

        // Return if command does not exist
        if (!this.client.commands.has(command)) return;

        // Retrieve and run command 
        this.client.commands.get(command).run(message, args);
    }
}

module.exports = Message;