'use strict';

/**
 * Ready class
 * @class
 */
class Ready {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        this.client = client;
    }

    /**
     * Run method
     */
    async run() {
        // Log online message
        this.client.logger.ready(`${this.client.user.username} is now online on ${this.client.guilds.size} server(s).`);

        // Generate bot invite link
        // const link = await this.client.generateInvite(2146827718);
        // this.client.logger.log(link);

        // Set bot default activity
        this.client.user.setActivity('music', {
            type: 'LISTENING'
        });
    }
}

module.exports = Ready;