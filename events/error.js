'use strict';

/**
 * Error class
 * @class
 */
class BotError {
    /**
     * Constructor method
     * @constructor
     * @param {Client} client 
     */
    constructor(client) {
        this.client = client;
    }

    /**
     * Run method
     * @param {Error} message
     * @memberof Error
     */
    async run(error) {
        this.client.logger.error(`${error.name}: ${error.message}`);
    }
}

module.exports = BotError;