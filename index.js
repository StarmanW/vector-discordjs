'use strict';

/**
 * Check if installed NodeJS version is higher than v8.0.0,
 * otherwise throw error to inform the user.
 */
if (Number(process.version.slice(1).split(".")[0]) < 8) throw new Error("Node 8.0.0 or higher is required. Please update Node on your system.");

// Require and instantiate an instance of the bot
const Vector = require('./includes/vector'),
    vector = new Vector();

// Run bot
vector.run();